package com.weathertask.data.pref

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

/**
 * @author Abhishek Kumar
 */

class AppPreferenceHelper : PreferencesHelper {

    private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    private var mPrefs: SharedPreferences? = null

    @Inject
    constructor(context: Context, @PreferenceInfo prefFileName: String){
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
    }



    override var accessToken: String
        get() = mPrefs!!.getString(PREF_KEY_ACCESS_TOKEN, null)!!
        set(value) {
            mPrefs!!.edit().putString(PREF_KEY_ACCESS_TOKEN, value).apply()
        }


}