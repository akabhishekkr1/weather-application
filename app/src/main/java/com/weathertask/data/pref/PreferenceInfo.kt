package com.weathertask.data.pref

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier

/**
 * @author Abhishek Kumar
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class PreferenceInfo