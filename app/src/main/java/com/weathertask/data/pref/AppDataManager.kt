package com.weathertask.data.pref

import com.weathertask.data.network.DataManager
import javax.inject.Inject

/**
 * @author Abhishek Kumar
 */

class AppDataManager : DataManager {


    private var mPreferencesHelper: PreferencesHelper? = null

    @Inject
    constructor(preferencesHelper: PreferencesHelper)
    {
        mPreferencesHelper = preferencesHelper
    }

    override fun updateUserInfo(AccessToken: String, userName: String, email: String, profilePicPath: String) {
        accessToken = AccessToken

    }

     var accessToken: String
        get() = mPreferencesHelper!!.accessToken
        set(accesstoken) {
            mPreferencesHelper!!.accessToken = accesstoken
        }
}