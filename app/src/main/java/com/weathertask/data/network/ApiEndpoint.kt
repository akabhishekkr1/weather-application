package com.weathertask.data.network;

import com.weathertask.model.currentdata.CurrentWeatherData
import com.weathertask.model.futuredata.FutureWeatherData
import com.weathertask.utils.API_KEY
import io.reactivex.Observable

import retrofit2.Call
import retrofit2.http.*


/**
 * @author Abhishek Kumar
 */

interface ApiEndpoint {

    @GET("weather?q=bangalore&appid=$API_KEY")
    fun getCurrentData() : Observable<CurrentWeatherData>

    @GET("forecast?q=bangalore&cnt=40&appid=$API_KEY")
    fun getFutureData() : Observable<FutureWeatherData>

}