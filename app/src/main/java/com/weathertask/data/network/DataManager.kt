package com.weathertask.data.network;

/**
 * @author Abhishek Kumar
 */

interface DataManager  {

    fun updateUserInfo(
            accessToken: String,
            userName: String,
            email: String,
            profilePicPath: String)


}