package com.weathertask.data.navigator

/**
 * @author Abhishek Kumar
 */

interface ResponseNavigators {
    fun onSuccess(any: Any)
    fun onError(error: String)
}