package com.weathertask.viewmodel

import com.weathertask.di.module.header.HeaderData
import com.weathertask.data.network.ApiEndpoint
import com.weathertask.data.network.DataManager
import dagger.Module
import dagger.Provides

/**
 * @author Abhishek Kumar
 */

@Module
class HomeModule {
    @Provides
    fun homeViewModelProvider(
        apiService: ApiEndpoint,
        dataManager: DataManager,
        headerData: HeaderData
    ): HomeViewModel {
        return HomeViewModel(apiService, dataManager, headerData)
    }
}