package com.weathertask.viewmodel

import com.weathertask.base.BaseViewModel
import com.weathertask.di.module.header.HeaderData
import com.weathertask.model.currentdata.CurrentWeatherData
import com.weathertask.model.futuredata.FutureWeatherData
import com.weathertask.data.network.ApiEndpoint
import com.weathertask.data.network.DataManager
import com.weathertask.data.navigator.ResponseNavigators
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Abhishek Kumar
 */

class HomeViewModel(
    apiService: ApiEndpoint,
    dataManager: DataManager,
    headerData: HeaderData
) : BaseViewModel<ResponseNavigators>(
    dataManager, apiService, headerData
) {

    fun getCurrentData() {
        getApiService()!!.getCurrentData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CurrentWeatherData> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: CurrentWeatherData) {
                    getNavigator()!!.onSuccess(t)

                }

                override fun onError(e: Throwable) {
                    getNavigator()!!.onError("Something Went Wrong")
                }

            })
    }


    fun getFutureData() {
        getApiService()!!.getFutureData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<FutureWeatherData> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: FutureWeatherData) {
                    getNavigator()!!.onSuccess(t)

                }

                override fun onError(e: Throwable) {
                    getNavigator()!!.onError("Something Went Wrong")
                }

            })


    }

}