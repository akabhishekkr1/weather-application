package com.weathertask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.weathertask.R
import com.weathertask.databinding.ItemsLayoutBinding
import com.weathertask.model.futuredata.X
import com.weathertask.utils.Itemclicklistener
import com.weathertask.utils.Util
import kotlinx.android.synthetic.main.items_layout.view.*
import kotlin.math.roundToInt

/**
 * @author Abhishek Kumar
 */

class WeatherAdapter(var context : Context, var items: List<X>, var itemClick: Itemclicklistener) :
    RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    var binding: ItemsLayoutBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.items_layout, parent, false)
        return ViewHolder(binding!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.temp.text =
            Util.kelvinToCelsius(items[position].main.temp).roundToInt().toString()
        holder.itemView.date.text =
            items[position].dt_txt.substring(8, 10) + "- Feb"

        holder.itemView.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(
                context,
                R.anim.slide_up
            )
            animation.startOffset = (10 * position).toLong()
            holder.itemView.startAnimation(animation)
            itemClick.itemclick(position, items[position])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(binding: ItemsLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}