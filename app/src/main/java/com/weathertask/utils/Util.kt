package com.weathertask.utils

import java.text.SimpleDateFormat

/**
 * @author Abhishek Kumar
 */

object Util {
    fun kelvinToCelsius(kel : Double) : Double{
        var sal = kel - 273.15
        return sal
    }
    fun getDate(data :String ,formate : String ): String{
        val formate = SimpleDateFormat(formate)
        return  formate.format(data)
    }
}