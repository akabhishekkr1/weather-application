package com.weathertask.model.currentdata

data class Coord(
    val lat: Double,
    val lon: Double
)