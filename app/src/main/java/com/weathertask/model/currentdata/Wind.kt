package com.weathertask.model.currentdata

data class Wind(
    val deg: Int,
    val speed: Double
)