package com.weathertask.model.currentdata

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)