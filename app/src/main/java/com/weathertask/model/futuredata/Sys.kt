package com.weathertask.model.futuredata

data class Sys(
    val pod: String
)