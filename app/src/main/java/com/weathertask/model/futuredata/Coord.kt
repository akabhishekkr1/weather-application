package com.weathertask.model.futuredata

data class Coord(
    val lat: Double,
    val lon: Double
)