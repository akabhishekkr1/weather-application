package com.weathertask.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.weathertask.R
import com.weathertask.base.BaseActivityBinding
import com.weathertask.databinding.ActivitySplashScreenBinding
import com.weathertask.utils.DURATION_3000


/**
 * @author Abhishek Kumar
 */

class SplashScreenActivity : BaseActivityBinding() {

    lateinit var binding : ActivitySplashScreenBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@SplashScreenActivity,R.layout.activity_splash_screen)
        handler()
    }

    fun handler(){
        Handler().postDelayed({
                val mainIntent = Intent(this@SplashScreenActivity, HomeActivity::class.java)
                this@SplashScreenActivity.startActivity(mainIntent)
                this@SplashScreenActivity.finish()

        }, DURATION_3000)
    }
}
