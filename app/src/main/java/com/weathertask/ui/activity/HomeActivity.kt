package com.weathertask.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.weathertask.R
import com.weathertask.adapter.WeatherAdapter
import com.weathertask.base.BaseActivityBinding
import com.weathertask.databinding.ActivityHomeBinding
import com.weathertask.model.currentdata.CurrentWeatherData
import com.weathertask.model.futuredata.FutureWeatherData
import com.weathertask.model.futuredata.X
import com.weathertask.utils.Itemclicklistener
import com.weathertask.data.navigator.ResponseNavigators
import com.weathertask.utils.Util.kelvinToCelsius
import com.weathertask.viewmodel.HomeViewModel
import javax.inject.Inject
import kotlin.math.roundToInt

/**
 * @author Abhishek Kumar
 */

class HomeActivity : BaseActivityBinding(),
    ResponseNavigators, Itemclicklistener, View.OnClickListener {

    @Inject
    lateinit var viewModel: HomeViewModel

    lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@HomeActivity, R.layout.activity_home)
        viewModel!!.setNavigator(this)
        binding.homeViewModel = viewModel
        binding!!.retryBtn.setOnClickListener(this)
        viewModel!!.getCurrentData()
        viewModel!!.getFutureData()
    }

    override fun onSuccess(any: Any) {
        if (any != null) {
            if (any is CurrentWeatherData) {
                var currentWeatherData = any as CurrentWeatherData
                setCurrentInfo(currentWeatherData)
            } else if (any is FutureWeatherData) {
                var futureWeatherData = any as FutureWeatherData
                setFutureInfo(futureWeatherData)
            }
        }
    }

    private fun setFutureInfo(futuredata: FutureWeatherData) {
        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.GONE
        binding.retryBtn.visibility = View.GONE
        binding.mainLl.visibility = View.VISIBLE
        var list = futuredata.list
        var newList = list.distinctBy { it.dt_txt.substring(0, 10) }
        newList = newList.subList(1, newList.size)
        binding.recyceler.adapter = WeatherAdapter(this, newList, this)
    }

    private fun setCurrentInfo(currentWeatherData: CurrentWeatherData) {
        binding.temp.text = kelvinToCelsius(currentWeatherData.main.temp).roundToInt().toString()
        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.GONE
        binding.retryBtn.visibility = View.GONE
        binding.mainLl.visibility = View.VISIBLE
    }

    override fun onError(error: String) {
        binding.progressCircular.visibility = View.GONE
        binding.noTv.visibility = View.VISIBLE
        binding.retryBtn.visibility = View.VISIBLE
        binding.mainLl.visibility = View.GONE
        Toast.makeText(this@HomeActivity, error, Toast.LENGTH_LONG).show()
    }

    override fun itemclick(int: Int, any: Any?) {
        var data = any as X
        binding.temp.text = Math.round(kelvinToCelsius(data.main.temp)).toString()
        binding.date.text = data.dt_txt.substring(8, 10) + "- Feb"

    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.retry_btn ->{
                binding.progressCircular.visibility = View.VISIBLE
                binding.noTv.visibility = View.GONE
                binding.retryBtn.visibility = View.GONE
                binding.mainLl.visibility = View.GONE
                viewModel!!.getCurrentData()
                viewModel!!.getFutureData()
            }
        }
    }
}
