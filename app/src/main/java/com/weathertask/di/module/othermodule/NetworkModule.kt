package com.weathertask.di.module.othermodule
import com.google.gson.GsonBuilder
import com.weathertask.data.network.ApiEndpoint
import com.weathertask.utils.BASE_URL

import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * @author Abhishek Kumar
 */

@Module
object NetworkModule {

    @Reusable
    @Provides
    @JvmStatic
    internal fun providePostApi(retrofit: Retrofit): ApiEndpoint {
        return retrofit.create(ApiEndpoint::class.java)
    }


    @Provides
    @JvmStatic
    @Reusable
    internal fun provideRetrofitInterface(): Retrofit {
        var client =
                OkHttpClient().newBuilder()
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(15, TimeUnit.SECONDS)
                        .build()

        var gson = GsonBuilder()
                .setLenient()
                .create()

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(client)
                .build()

    }


}