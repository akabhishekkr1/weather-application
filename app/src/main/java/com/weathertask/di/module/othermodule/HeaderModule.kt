package com.weathertask.di.module.othermodule
import com.weathertask.di.module.header.HeaderData
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Abhishek Kumar
 */

@Module
class HeaderModule {

    @Provides
    @Singleton
    fun provideHeader(): HeaderData {
        return HeaderData()
    }
}