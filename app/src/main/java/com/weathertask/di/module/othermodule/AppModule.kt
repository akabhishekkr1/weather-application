package com.weathertask.di.module.othermodule
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.weathertask.data.network.DataManager
import com.weathertask.data.pref.AppDataManager
import com.weathertask.data.pref.AppPreferenceHelper
import com.weathertask.data.pref.PreferenceInfo
import com.weathertask.data.pref.PreferencesHelper
import com.weathertask.utils.*

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Abhishek Kumar
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }


    @Provides
    @Singleton
    internal fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }


    @Provides
    @PreferenceInfo
    internal fun providePreferenceName(): String {
        return PREF_NAME
    }


    @Provides
    @Singleton
    fun provideSharedPreference(app: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Provides
    @Singleton
    internal fun providePreferencesHelper(appPreferencesHelper: AppPreferenceHelper): PreferencesHelper {
        return appPreferencesHelper
    }


}