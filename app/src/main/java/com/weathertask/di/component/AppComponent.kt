package com.weathertask.di.component
import android.app.Application
import com.weathertask.ui.application.App
import com.weathertask.di.builder.ActivityBuilder
import com.weathertask.di.module.othermodule.AppModule
import com.weathertask.di.module.othermodule.HeaderModule
import com.weathertask.di.module.othermodule.NetworkModule

import dagger.BindsInstance
import dagger.Component


import javax.inject.Singleton


/**
 * @author Abhishek Kumar
 */

@Singleton
@Component(modules = [ ActivityBuilder::class, NetworkModule::class, AppModule::class, HeaderModule::class  ])
interface AppComponent  {

    fun inject(app: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}