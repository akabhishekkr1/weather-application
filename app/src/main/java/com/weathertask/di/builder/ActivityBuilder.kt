package com.weathertask.di.builder

import com.weathertask.ui.activity.HomeActivity
import com.weathertask.ui.activity.SplashScreenActivity
import com.weathertask.viewmodel.HomeModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Abhishek Kumar
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun splashActivity() : SplashScreenActivity

    @ContributesAndroidInjector(modules = [(HomeModule::class)])
    abstract fun homeActivity(): HomeActivity

}